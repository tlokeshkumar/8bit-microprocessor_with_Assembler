`timescale 1ns / 1ps
////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date:   07:15:18 10/27/2017
// Design Name:   instruction_controller
// Module Name:   F:/IIT Madras/Academics/Semester 3/microporcessor/sample/sample_project/testin_1.v
// Project Name:  sample_project
// Target Device: 
// Tool versions: 
// Description:
//
// Verilog Test Fixture created by ISE for module: instruction_controller
// 
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
////////////////////////////////////////////////////////////////////////////////
module testin_1;
 // Inputs
 
 //reg [7:0] c_in;
 reg  clk;//, jump;
 //reg [7:0] opcode;
 //reg [2:0] abus_1, abus_2;
 //reg enable_alu;
 // Outputs
 //wire [2:0] alu_fl;
 //wire [3:0] c_out;
 wire [7:0] result;
 wire [3:0] flag;
 //wire [7:0] out_dbus_1, out_dbus_2;
 
 // Instantiate the Unit Under Test (UUT)
 processor uut (
  .clk(clk),
  //.opcode(opcode),
  //.abus_1(abus_1),
  //.abus_2(abus_2),
  //.out_dbus_1(out_dbus_1),
  //.out_dbus_2(out_dbus_2)
  .result(result),
  .flag(flag)
  );
 initial begin
    $dumpfile("test_2.vcd");
    $dumpvars(0,testin_1);
  // Initialize Inputs
  clk = 0;
 repeat(100)#100 clk = ~clk;
  end

     
endmodule