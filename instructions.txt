00000000 ADD
00000001 SUB
00000010 BITWISE_AND
00000011 BITWISE_OR
00000100 LEFT_SHIFT
00000101 RIGHT_SHIFT
00000110 LOAD
00000111 CMP
00001000 MOV
00001001 IF LX = x JUMP xxxx
00001010 HALT
























//begin
// Division of 2 Numbers
/*
This code to divide 2 numbers can return the possible outcomes
If the number is not divisible = The last but 1 and 2 in the simulation (result) is the r and q.
If the number is exactly divisible = The last and the last but one in the simulation (result) is r and q
*/
dm_array[0] = 22'b0011001000000110000001; //ld r0 50 
dm_array[1] = 22'b0000101000000110001000; //ld r1 10
dm_array[2] = 22'b0000000000000110010000; //ld r2 0
dm_array[3] = 22'b0000000100000110011000; //ld r3 1
dm_array[4] = 22'b0000000000000001000001; //sub r0 r1
dm_array[5] = 22'b0000000000001000100000; //mov r4 r0
dm_array[6] = 22'b0000000000000000010011; //add r2 r3
dm_array[7] = 22'b0000000000000111000001; //cmp r0 r1
dm_array[8] = 22'b0000010000001001011001; //if l3 = 1 jump 4
dm_array[9] = 22'b0000000000000001100001; //sub r4 r1
dm_array[10]= 22'b0000110100001001010001; // if l2 = 1 jump this_line +2
dm_array[11]= 22'b0000000000000001000001; // sub r0 r1
dm_array[12]= 22'b0000000000000000010011; // add r2 r3
dm_array[13]= 22'b0000000000001010000000; // halt
//end








// program to generate factorial of a given number
//begin
dm_array[0] = 22'b0000011000000110000001; //ld r0 5 
dm_array[1] = 22'b0000000100000110001000; //ld r1 1
dm_array[2] = 22'b0000000000000110010000; //ld r2 0
dm_array[3] = 22'b0000000000001000101010; //mov r5 r2
dm_array[4] = 22'b0000000000001000011000; //mov r3 r0
dm_array[5] = 22'b0000010100000001011001; //sub r3 r1
dm_array[6] = 22'b0000000000001000110011; // mov r6 r3
dm_array[7] = 22'b0000000000000001110001; //sub r6 r1
dm_array[8] = 22'b0000000000001000100000; //mov r4 r0
dm_array[9] = 22'b0000000000000000010100; //add r2 r4
dm_array[10]= 22'b0000010100000001011001; //sub r3 r1
dm_array[11]= 22'b0000100100001001010001; //if l2=1 jump 9
dm_array[12]= 22'b0000000000001000100010; //mov r4 r2
dm_array[14]= 22'b0000001100001000011110; //mov r3 r0
dm_array[13]= 22'b0000000000000001110001; //sub r6 r1
dm_array[15]= 22'b0000100100001001010001; //if l2=1 jump 9
dm_array[16]= 22'b0000000000000000101010; //add r5 r2
dm_array[17]= 22'b0000000000001010000000; //halt
//end


