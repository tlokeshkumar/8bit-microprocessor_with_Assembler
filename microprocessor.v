//`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    22:28:46 10/26/2017 
// Design Name: 
// Module Name:    alu 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
//`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    22:16:48 10/26/2017 
// Design Name: 
// Module Name:    mini_2 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
//`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    21:54:26 10/26/2017 
// Design Name: 
// Module Name:    alu 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    22:15:26 10/24/2017 
// Design Name: 
// Module Name:    sample_verilog 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module alu(clk,en,a, b, outp, control_bus, flag);
input [7:0] a;
input [7:0] b;
input [2:0] control_bus;
output reg [7:0] outp;
output reg [3:0] flag;
input en;
input clk;
reg [8:0] temp_a;
//always @ (en or a or b or outp or control_bus)
always@(*)
if(en)
begin
case(control_bus)
3'b000:
begin
temp_a <= a+b;
flag[0] <= temp_a[8];
outp <= a+b;

if (outp == 0) 
begin
flag[2] <= 1'b0;
end
else 
begin
flag[2] <= 1'b1;
end

end
3'b001:
begin
outp <= a-b;
$display("output is 0");
// if (b > a) 
// flag[0] <= 1'b1;
// else flag[0] <= 1'b0;
temp_a <= a-b;
flag[0] <= temp_a[8];
if (outp == 0) 
begin
flag[2] <= 1'b0;
end
else 
begin
flag[2] <= 1'b1;
end
end

3'b010:
begin
outp <= a&b;
if (outp == 0) 
begin
flag[2] <= 1'b0;
end
else 
begin
flag[2] <= 1'b1;
end
end

3'b011:
begin
outp <= a|b;
if (outp == 0) 
begin
flag[2] <= 1'b0;
end
else 
begin
flag[2] <= 1'b1;
end
end

3'b100:
begin
flag[1] <= a[7];
outp <= a<<1;
if (outp == 0) 
begin
flag[2] <= 1'b0;
end
else 
begin
flag[2] <= 1'b1;
end
end
3'b101:
begin
flag[1] <= a[0];
outp <= a>>1;
if (outp == 0) 
begin
flag[2] <= 1'b0;
end
else 
begin
flag[2] <= 1'b1;
end
end
3'b110:
begin
//compare r1 r2 #(flag is 1 if r1>r2, else 0)
if (a>b) 
begin
flag[3] = 1'b1;
end
else
begin
flag[3] = 1'b0;
end
end 
endcase
end
endmodule

module RAM(
input clk,
input en,
input rd, wr,
input [2:0] abus,
input [7:0] in_dbus,
output reg [7:0] out_dbus
);
reg [7:0] dm_array [0:5];
always@(posedge clk)
begin

// $display("The value of the register 0 is %b", dm_array[0]);
// $display("The value of the register 1 is %b", dm_array[1]);
if(en)
begin
if(rd)
begin
$display("read at %b , %b", abus, out_dbus);
out_dbus=dm_array[abus];
$display("sanity check %b, %b", dm_array[abus], out_dbus);
end
end
if(en)
begin
if(wr)
begin
$display("wrote at %b , %b", abus, in_dbus);
dm_array[abus]=in_dbus;
$display("sanity check %b, %b", dm_array[abus], in_dbus);
end
end
end
initial
begin
dm_array[0] = 8'b00000001;
dm_array[1] = 8'b00000000;
dm_array[2] = 8'b00000010;
dm_array[3] = 8'b00001001;
end
endmodule
/*
module read_write(
input clk,rdwr
input [2:0] abus,
input [7:0] in_bus,
output [7:0] out_bus
);
DataMemory dataSample(.clk(clk), .en(en),.rd(rdwr), .wr(~rdwr), .abus(abus),.out_dbus(out_dbus),.in_dbus(in_dbus));
//always@(en, rdwr, adbus_1, adbus_2, in_dbus_1, in_dbus_2)
always@(posedge clk)
begin
rdwr = 1'b1;
en = 1'b1;
end
initial begin
en = 1'b0;
end
endmodule
*/

module program_counter(
    input [7:0] c_in,
    input clk,jump,
    output reg[7:0] opcode,
    output  reg[2:0] ad_1, ad_2,
    output reg [7:0] data_load,
    output reg [7:0] c_out
);
reg [21:0] dm_array [0:20];
//assign ad_2 = dm_array[c_out][2:0];
//assign ad_1 = dm_array[c_out][5:3];
//assign opcode = dm_array[c_out][13:5];
//instruction_controller ins_control_instance(.opcode(opcode), .op_1(op_1), .op_2(op_2),.clk(clk), .result(result));
always@(posedge clk)
//if (jump) c_out = c_in;
begin
c_out <= c_in;
ad_2 <= dm_array[c_in][2:0];
ad_1 <= dm_array[c_in][5:3];
opcode <= dm_array[c_in][13:6];
data_load <= dm_array[c_in][21:14];
end
//else 
//begin
//count_next = c_out + 8'b00000001; 
//c_out <= c_out + 1;
//result = dm_array[c_out][3:0];
//end
initial $readmemb("binary_program.txt", dm_array);
endmodule

module instruction_controller(
input [7:0] opcode,
input [2:0] abus_1, abus_2,
//input [7:0] op_1, op_2,
input clk,
input [7:0] data_load,
input [7:0] pc_init,
//input  enable_alu,
//output [2:0] alu_fl,
output [7:0] result,
output [3:0] flag,
output reg [7:0] pc_update
//output reg [2:0] address_bus
);
reg [7:0] dm_array [0:10];
reg enable_alu, enable_ram, rdwr;
reg [2:0] alu_fl;
reg [2:0] adbus_temp;
reg [3:0] state;
//reg [7:0] op_1, op_2;
reg [7:0] op_1, op_2;
reg [7:0] in_dbus_m;
reg [2:0] address_bus_m;
wire [7:0] out_bus_m;
//DataMemory dataSample(.clk(clk), .en(en),.en_w1(en_w1),.rd(rdwr), .wr(~rdwr), .abus_1(abus_1),.abus_2(abus_2), .out_dbus_1(op_1),.out_dbus_2(op_2), .in_dbus_1(in_dbus_1), .in_dbus_2(in_dbus_2));
initial 
begin
enable_alu = 1'b0;
enable_ram = 1'b0;
//pc_init = 0;
end
//initial en = 1'b0;
alu alu_instance(.clk(clk),.en(enable_alu),.a(op_1),.b(op_2),.outp(result),.control_bus(alu_fl),.flag(flag));
RAM ram_instance(.clk(clk), .en(enable_ram), .rd(rdwr), .wr(~rdwr),.in_dbus(in_dbus_m), .abus(address_bus_m), .out_dbus(out_bus_m));
//assign alu_fl = 0;
// wires from alu_fl must be connected to the alu control bus(function select lines)
//always@(opcode or op_1 or op_2 or enable_alu)
initial begin
//assign rdwr = 1'b1;
//assign en = 1'b1;
dm_array[0] = 8;
dm_array[1] = 1;
dm_array[2] = 2;
dm_array[3] = 1;
dm_array[4] = 6;
dm_array[5] = 0;
end
always@(posedge clk)
begin
// op_1 <= dm_array[abus_1];
// op_2 <= dm_array[abus_2];
state <= 0;
case(opcode)
8'b00000000:
begin
// ADD operation
/*op_1 = dm_array[abus_1];
op_2 = dm_array[abus_2];
alu_fl= 3'b000;
enable_alu = 1'b1;
adbus_temp <= abus_1;
//adbus_temp <= adbus_temp;
dm_array[adbus_temp] <= result;
// $display("writing at %b this %b ", adbus_temp, result);
//100;
pc_update = pc_init + 1;
*/
// $display("pc_init %b", pc_init);
//rd = 1'b1;
//address_bus=ad_1;
//state <=0;
if (state == 0)
begin
op_1 <= dm_array[abus_1];
$display("Adding state 0 %b", dm_array[abus_1]);
state <= 4'b0001;
end
if (state == 4'b0001)
begin 
op_2 <= dm_array[abus_2];
$display("Adding state 1");
state <= 4'b0010;
end
 if (state == 4'b0010)
begin 
alu_fl <= 3'b000;
enable_alu <= 1'b1;
state <= 4'b0011;
$display("Adding state 2 ");
end
if (state == 4'b0011)
begin 
dm_array[abus_1] <= result;
enable_alu <= 1'b0;
state <= 4'b0100;
$display("Adding state 3");
end 
if (state == 4'b0100) 
begin 
pc_update <= pc_init + 1;
$display("Adding state 4");
state <= 4'b1111;
end
if (state == 4'b1111)
begin

end

//a=op_bus;
//address_bus=ad_2;
//b=op_bus;
//enable = 1'b0;
//a=op1;
//b=op2;
end
8'b00000001:
begin
// SUBTRACTION operation
/*op_1 <= dm_array[abus_1];
op_2 <= dm_array[abus_2];
alu_fl<=3'b001;
//enable = 1'b1;
//address_bus=ad_1;/
//a=op_bus;
enable_alu <= 1'b1;
//#100;
adbus_temp <= abus_1;       
dm_array[adbus_temp] <= result;
pc_update = pc_init + 1;
$display("pc_update %b", pc_update);
$display("pc_init %b", pc_init);
//address_bus=ad_2;
//b=op_bus;
//enable = 1'b0;
*/

//state <=0;
if (state == 0)
begin
op_1 <= dm_array[abus_1];
state <= 4'b0001;
$display("Sub state 0");
end
if (state == 4'b0001)
begin 
op_2 <= dm_array[abus_2];
$display("Sub state 1");
state <= 4'b0010;
end
 if (state == 4'b0010)
begin 
alu_fl <= 3'b001;
enable_alu <= 1'b1;
$display("Sub state 2");
state <= 4'b0011;
end
if (state == 4'b0011)
begin 
enable_alu <= 1'b0;
//#100;
dm_array[abus_1] <= result;
$display("Sub state 3");
state <= 4'b0100;
end 
if (state == 4'b0100) 
begin 
pc_update <= pc_init + 1;
state <= 4'b1111;
$display("Sub state 4");
end
if (state == 4'b1111)
begin

end
end

8'b00000010:
begin
// bitwise and
/*op_1 <= dm_array[abus_1];
op_2 <= dm_array[abus_2];
alu_fl<=3'b010;
enable_alu <= 1'b1;
adbus_temp <= abus_1;
dm_array[adbus_temp] <= result;
pc_update = pc_init + 1;
$display("pc_update %b", pc_update);
$display("pc_init %b", pc_init);
//enable = 1'b1;
//address_bus=ad_1;
//a=op_bus;
//address_bus=ad_2;
//b=op_bus;
//enable = 1'b0;
*/
//state <=0;
if (state == 0)
begin
op_1 <= dm_array[abus_1];
state <= 4'b0001;
end
if (state == 4'b0001)
begin 
op_2 <= dm_array[abus_2];
state <= 4'b0010;
end
 if (state == 4'b0010)
begin 
alu_fl <= 3'b010;
enable_alu <= 1'b1;
state <= 4'b0011;
$display("%d", op_1);
$display("%d", op_2);
$display("%d", abus_1);
end
if (state == 4'b0011)
begin 
dm_array[abus_1] <= result;
enable_alu <= 1'b0;
state <= 4'b0100;
end 
if (state == 4'b0100) 
begin 
pc_update <= pc_init + 1;
state <= 4'b1111;
end
if (state == 4'b1111)
begin

end
end

8'b00000011:
begin
// bitwise or
/*op_1 <= dm_array[abus_1];
op_2 <= dm_array[abus_2];
alu_fl<=3'b011;
enable_alu <= 1'b1;
adbus_temp <= abus_1;
dm_array[adbus_temp] <= result;
pc_update = pc_init + 1;
$display("pc_update %b", pc_update);
$display("pc_init %b", pc_init);
//enable = 1'b1;
//address_bus=ad_1;
//a=op_bus;
//address_bus=ad_2;
//b=op_bus;
//enable = 1'b0;
*/
//state <=0;

/*
if (state == 0)
begin
op_1 <= dm_array[abus_1];
state <= 4'b0001;
end
if (state == 4'b0001)
begin 
op_2 <= dm_array[abus_2];
state <= 4'b0010;
end
 if (state == 4'b0010)
begin 
alu_fl <= 3'b011;
enable_alu <= 1'b1;
state <= 4'b0011;
$display("%d", op_1);
$display("%d", op_2);
end
if (state == 4'b0011)
begin 
dm_array[abus_1] <= result;
enable_alu <= 1'b0;
state <= 4'b0100;
end 
if (state == 4'b0100) 
begin 
$display("%d", dm_array[abus_1]);
pc_update <= pc_init + 1;
state = 4'b1111;
end
if (state == 4'b1111)
begin

end
*/
if (state == 0)
begin
op_1 <= dm_array[abus_1];
state <= 4'b0001;
end
if (state == 4'b0001)
begin 
op_2 <= dm_array[abus_2];
state <= 4'b0010;
end
 if (state == 4'b0010)
begin 
alu_fl <= 3'b011;
enable_alu <= 1'b1;
state <= 4'b0011;
$display("%d", op_1);
$display("%d", op_2);
$display("%d", abus_1);
end
if (state == 4'b0011)
begin 
dm_array[abus_1] <= result;
enable_alu <= 1'b0;
state <= 4'b0100;
end 
if (state == 4'b0100) 
begin 
pc_update <= pc_init + 1;
state <= 4'b1111;
end
if (state == 4'b1111)
begin

end

end
8'b00000100:
begin
// left shift
/*op_1 <= dm_array[abus_1];
op_2 <= dm_array[abus_2];
 alu_fl <= 3'b100;
enable_alu <= 1'b1;
adbus_temp <= abus_1;
dm_array[adbus_temp] <= result;
pc_update = pc_init + 1;
$display("pc_update %b", pc_update);
$display("pc_init %b", pc_init);
*/
//state <=0;
if (state == 0)
begin
op_1 <= dm_array[abus_1];
state <= 4'b0001;
end
if (state == 4'b0001)
begin 
alu_fl <= 3'b100;
enable_alu <= 1'b1;
state <= 4'b0010;
end
 if (state == 4'b0010)
begin 
dm_array[abus_1] <= result;
enable_alu <= 1'b0;
state <= 4'b0011;
end
if (state == 4'b0011)
begin 
pc_update <= pc_init + 1;
state <= 4'b1111;
end 
if (state == 4'b1111)
begin

end//enable = 1'b1;
//address_bus=ad_1;
//a=op_bus;
//address_bus=ad_2;
//enable = 1'b0;
end
8'b00000101:
begin
// right shift
/*op_1 <= dm_array[abus_1];
op_2 <= dm_array[abus_2];
alu_fl <= 3'b101;
enable_alu <= 1'b1;
adbus_temp <= abus_1;
dm_array[adbus_temp] <= result;
pc_update = pc_init + 1;
$display("pc_update %b", pc_update);
$display("pc_init %b", pc_init);
*///enable = 1'b1;
//address_bus=ad_1;
//a=op_bus;
//
//enable = 1'b0;
//state <=0;
if (state == 0)
begin
op_1 <= dm_array[abus_1];
state <= 4'b0001;
end
if (state == 4'b0001)
begin 
alu_fl <= 3'b101;
enable_alu <= 1'b1;
state <= 4'b0010;
end
 if (state == 4'b0010)
begin 
dm_array[abus_1] <= result;
enable_alu <= 1'b0;
state <= 4'b0011;
end
if (state == 4'b0011)
begin 
pc_update <= pc_init + 1; 
state <= 4'b1111;
end 
if (state == 4'b1111)
begin

end
end
8'b00000110:
begin
//Load into memory

//enable_alu <= 1'b0;
// $display("dm_array[adbus_temp] %b", dm_array[adbus_temp]);
if (state == 4'b0000)
begin
dm_array[abus_1] <= data_load;
state <= 4'b0001;
end
// $display("dm_array[adbus_temp] %b", dm_array[adbus_temp]);
if (state == 4'b0001)
begin
pc_update <= pc_init + 1;
state <= 4'b1111;
end
// $display("pc_init %b", pc_init);

/*
state <=0;
if (state == 0)
begin
//adbus_temp <= abus_1;
dm_array[abus_1] = data_load;
$display("writtenword %b", dm_array[abus_1]);
state <= 4'b0001;
end
if (state == 4'b0001)
begin 
pc_update <= pc_init + 1;
end
*/
end

8'b00000111:
begin
//compare r1 r2 #(flag is 1 if r1>r2, else 0)
/*op_1 <= dm_array[abus_1];
op_2 <= dm_array[abus_2];
alu_fl <= 3'b110;
enable_alu <= 1'b1;
// adbus_temp <= abus_1;
// dm_array[adbus_temp] <= result;
pc_update = pc_init + 1;
$display("pc_update %b", pc_update);
$display("pc_init %b", pc_init);
*/
//state <=0;
if (state == 0)
begin
op_1 <= dm_array[abus_1];
$display("cmp state 0");
state <= 4'b0001;
end
if (state == 4'b0001)
begin 
op_2 <= dm_array[abus_2];
$display("cmp state 1");
state <= 4'b0010;
end
 if (state == 4'b0010)
begin 
alu_fl <= 3'b110;
enable_alu <= 1'b1;
$display("cmp state 2");
state <= 4'b0011;
end
if (state == 4'b0011)
begin 
$display("cmp state 3");
enable_alu <= 1'b0;
pc_update <= pc_init + 1;
state <= 4'b1111;
end

end

8'b00001000:
begin
// MOV R1, R2 (Contents of R2 is moved to R1)
if (state == 4'b0000)
    begin
    op_1 <= dm_array[abus_1];
    state = 4'b0001;
    end
if (state == 4'b0001)
    begin
    op_2 <= dm_array[abus_2];
    state <= 4'b0010;
    end
if (state == 4'b0010)
    begin
    dm_array[abus_1] = op_2;
    state <= 4'b0011;
    end
if (state == 4'b0011)
    begin
    pc_update <= pc_init + 1;
    state <= 4'b1111;
    end
//adbus_temp <= abus_1;


/*
state <=0;
if (state == 0)
begin
op_2 <= dm_array[abus_2];
//op_1 <= dm_array[abus_1];
dm_array[abus_1] <= op_2;
state <= 4'b0001;
end
if (state == 4'b0001)
begin 
state <= 4'b0010;
end
if (state == 4'b0010)
begin 
pc_update <= pc_init + 1;
end
*/
end

8'b00001001:
begin

// (where to jump)(opcode)(which flag)(what value) if (LX == value(abus_2)) JUMP data_load; //X = abus_1
/*op_1 <= dm_array[abus_1];
op_2 <= dm_array[abus_2];
adbus_temp <= abus_1;
if (flag[adbus_temp] == abus_2) 
begin
pc_update = data_load;
end
else 
begin
pc_update = pc_init + 1;
*/
state <=0;
if (state == 0)
begin
$display("state 0");
if (flag[abus_1] == abus_2[0])
begin
$display("jumping");
pc_update <= data_load; 
end
else
begin
pc_update <= pc_init + 1;
end
end 
end

// 8'b00001010:
// begin
// //indirect addressing
// // IA R3 (R4)
// state <= 0;
// if (state == 4'b000)
// begin

// end
// end

8'b00001010:
begin
//halt
// $display("halt");
end

8'b00001011:
begin
//store immediate instruction store 5 67 (in R5 of ram store value 67)
// (data to store)(opcode)(where to store in main memory)(x-dontcare-x)
if (state == 4'b0000)
    begin
    $display("stri state 0");
    address_bus_m <= abus_1;
    rdwr <= 1'b0;
    in_dbus_m <= data_load;
    state <= 4'b0001;
    end
if (state == 4'b0001)
    begin
    $display("stri state 1");
    enable_ram <= 1'b1;
    state <= 4'b0010;
    end
if (state == 4'b0010)
    begin
    $display("stri state 2");
    enable_ram <= 1'b0;
    pc_update <= pc_init + 1;
    state <= 4'b1111;
    end
end
8'b00001100:
begin
// load from ram to alu
//ldr r4 address_ram // this will store the value of the address in the ram to the specified reg
//(_x_dataload)(opcode)(alu_reg)(ram_reg)
if (state == 4'b0000)
    begin
    $display("ldr state 0");
    address_bus_m <= abus_2;
    rdwr <= 1'b1;
    state <= 4'b0001;
    end
if (state == 4'b0001)
    begin
    $display("ldr state 1");
    enable_ram <= 1'b1;
    state <= 4'b0010;
    end
if (state == 4'b0010)
    begin
    $display("ldr state 2");
    state <= 4'b0011;
    end
if (state == 4'b0011)
    begin
    $display("out_bus_m %d", out_bus_m);
    $display("ldr state 3");
    $display("%b", dm_array[abus_1]);
    $display("%b", abus_1);
    dm_array[abus_1] <= out_bus_m;
    enable_ram <= 1'b0;
    pc_update <= pc_init + 1;
    state <= 4'b1111;
    end
end
8'b00001101:
// str the value in the register(alu) to ram 
//(_x_data_load)(opcode)(alureg)(ramreg)
begin
if (state == 4'b0000)
    begin
    $display("str state 0");
    op_1 <= dm_array[abus_1];
    rdwr <= 1'b0;
    address_bus_m <= abus_2;
    state <= 4'b0001;
    end
if (state == 4'b0001)
    begin
    $display("str state 1");
    in_dbus_m <= op_1;
    state <= 4'b0010;
    end
if (state == 4'b0010)
    begin
    $display("str state 2");
    enable_ram <= 1'b1;
    state <= 4'b0011;
    end
if (state == 4'b0011)
    begin
    $display("str state 3");
    pc_update <= pc_init + 1;
    enable_ram <= 1'b0;
    state <= 4'b1111;
    end
end
8'b00001110:
// INC  
begin
if (state == 0)
begin
op_1 <= dm_array[abus_1];
$display("INC state 0");
state <= 4'b0001;
end
if (state == 4'b0001)
begin 
op_2 <= 8'b00000001;
$display("INC state 1 ");
state <= 4'b0010;
end
 if (state == 4'b0010)
begin 
alu_fl <= 3'b000;
enable_alu <= 1'b1;
state <= 4'b0011;
$display("INC state 2 ");
end
if (state == 4'b0011)
begin 
dm_array[abus_1] <= result;
enable_alu <= 1'b0;
state <= 4'b0100;
$display("INC state 3");
end 
if (state == 4'b0100) 
begin 
pc_update <= pc_init + 1;
$display("INC state 4");
state <= 4'b1111;
end
end
8'b00001111:
// DEC 
begin
if (state == 0)
begin
op_1 <= dm_array[abus_1];
$display("DEC state 0");
state <= 4'b0001;
end
if (state == 4'b0001)
begin 
op_2 <= 8'b00000001;
$display("DEC state 1 ");
state <= 4'b0010;
end
 if (state == 4'b0010)
begin 
alu_fl <= 3'b001;
enable_alu <= 1'b1;
state <= 4'b0011;
$display("DEC state 2 ");
end
if (state == 4'b0011)
begin 
dm_array[abus_1] <= result;
enable_alu <= 1'b0;
state <= 4'b0100;
$display("DEC state 3");
end 
if (state == 4'b0100) 
begin 
pc_update <= pc_init + 1;
$display("DEC state 4");
state <= 4'b1111;
end

end
endcase
end
endmodule

module processor(
    input clk,
    output [7:0] result,
    output [3:0] flag
);
wire [7:0] cout, opcode, c_temp, data_value;
wire [2:0] ad_1, ad_2;
reg [7:0]  cin;
program_counter pc_instance(.c_in(cin),.c_out(cout), .clk(clk), .opcode(opcode), .ad_1(ad_1), .ad_2(ad_2), .data_load(data_value));
instruction_controller ic_instance(.opcode(opcode),.data_load(data_value),.pc_init(cout), .pc_update(c_temp), .abus_1(ad_1), .abus_2(ad_2), .clk(clk), .result(result), .flag(flag));

always@(*)
begin
cin = c_temp;
end
initial begin cin = 0;
end
endmodule